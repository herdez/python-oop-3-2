
# OOP Shopping Cart Challenge

**BuyUs Startup** has to add for its Ecommerce APP a functionality of a shopping cart as well as defined methods that give it the mean and median prices of all the items in the cart. The shopping cart feature will be able to add items of different quantities and prices to the cart, calculate discounts, keep track of what items have been added, and void transactions.

***Chief technology innovation officer*** has requested a Proof of Concept (POC) that delivers the results in the requested format and the lines of code below should work and produce the previewed output.

## Proof of Concept

This POC Format must not be modified, coding must be in **shopping_cart.py** module.

This POC notebook has [%autoreload](https://ipython.readthedocs.io/en/stable/config/extensions/autoreload.html) packages so that when you update **shopping_cart.py**, IPython will reimport all the modules to make sure that you are using the latest possible versions. Remember that while the package will be reloaded, you will need to reinitialize your class instance.


```python
%load_ext autoreload
%autoreload 2
```


```python
# Import custom package
from shopping_cart import ShoppingCart
```


```python
# Initialize an instance of shopping cart class
shopping_cart = ShoppingCart()
```

## Initialization Behavior 

Three attributes are requested. The test samples must be ```True```.


```python
# Reinitialize instance of the class
shopping_cart = ShoppingCart()

print(shopping_cart.total == 0)
print(shopping_cart.employee_discount == None)
print(shopping_cart.items == [])
```

    True
    True
    True


##  `add_item()` method 

Instance method called `.add_item()` that will add an item to our cart. The method should increase the shopping cart's total by the appropriate amount and return the new total for the shopping cart. The test samples must be True.


```python
shopping_cart.add_item("rainbow sandals", 45.99) == 45.99
```




    True




```python
shopping_cart.add_item("agyle socks", 10.50) == 56.49
```




    True




```python
shopping_cart.add_item("jeans", 50.00, 3) == 206.49 
```




    True



##  `mean_item_price()` and `median_item_price()` methods

Two instance methods: `.mean_item_price()` and `.median_item_price()`, which should return the average price per item and the median price of the items in your cart, respectively. The test samples must be True.


```python
shopping_cart.mean_item_price() == 41.298
```




    True




```python
shopping_cart.median_item_price() == 50.00
```




    True



##  `apply_discount()` method 

Instance method called `.apply_discount()` that applies a discount if one is provided and returns the discounted total. The test samples must be True.


```python
discount_shopping_cart = ShoppingCart(20)
print("1. ", discount_shopping_cart.add_item("rainbow sandals", 45.00) == 45.00)
print("2. ", discount_shopping_cart.add_item("agyle socks", 15.00) == 60.00) 
print("3. ", discount_shopping_cart.apply_discount() == 48.00) 
print("4. ", discount_shopping_cart.add_item("macbook air", 1000.00) == 1060.00)
print("5. ", discount_shopping_cart.apply_discount() == 848.00) 
print("6. ", shopping_cart.apply_discount() == "Sorry, there is no discount to apply to your cart :(")
```

    1.  True
    2.  True
    3.  True
    4.  True
    5.  True
    6.  True


## `void_last_item()` method

A method called `void_last_item()` that removes the last item from the shopping cart and updates its total. The test samples must be True.


```python
print("1.  ", shopping_cart.void_last_item() == 50.00)
print("2.  ", shopping_cart.total == 156.49)
print("3.  ", shopping_cart.void_last_item() == 50.00)
print("4.  ", shopping_cart.total == 106.49)
print("5.  ", shopping_cart.void_last_item() == 50.00)
print("6.  ", shopping_cart.total == 56.49)
print("7.  ", shopping_cart.void_last_item() == 10.50)
print("8.  ", shopping_cart.total == 45.99)
print("9.  ", shopping_cart.void_last_item() == 45.99)
print("10. ", shopping_cart.total == 0)
print("11. ", shopping_cart.void_last_item() == "There are no items in your cart!")
print("12. ", shopping_cart.total == 0)
print("13. ", shopping_cart.void_last_item() == "There are no items in your cart!")
print("14. ", shopping_cart.total == 0)
```

    1.   True
    2.   True
    3.   True
    4.   True
    5.   True
    6.   True
    7.   True
    8.   True
    9.   True
    10.  True
    11.  True
    12.  True
    13.  True
    14.  True

